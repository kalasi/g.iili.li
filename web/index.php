<?php

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application;
$app['debug'] = true;

$app->get('/', function () {
    $range = [range('a', 'z'), range('A', 'Z'), range(0, 9)];

    array_walk_recursive($range, function($c) use (&$chars) {
        $chars .= $c;
    });

    require __DIR__ . './../views/index.html';

    return '';
});

$app->post('/', function (Request $request) {
    $c = $request->get('c');
    $l = $request->get('l');
    $json = $request->get('j');
    $j = $json == false || $json == "false" ? false : true;

    // Check if the character input is an array, and if so, de-array it.
    if (is_array($c)) {
        $c = array_flatten($c);
    }

    if (empty($c) || empty($l)) {
        return new Response('Both c and l are required.', 422);
    }

    $random = '';

    for ($i = 0; $i < $l; $i++) {
        $random .= $c[mt_rand(0, strlen($c) - 1)];
    }

    return $j ? json_encode($random) : $random;
});

$app->run();
